#!/bin/execlineb -P

if { s6-mkdir -p env }
s6-envdir env
backtick -E CONFIG_DIR {
	pipeline {
		if -nt { printenv CONFIG_DIR }
		printcontenv CONFIG_DIR
	}
	tee env/CONFIG_DIR
}
backtick -E CONFIG_FILE {
	pipeline {
		redirfd -w 2 /dev/null
		if -nt { printenv CONFIG_FILE }
		if -nt { printcontenv CONFIG_FILE }
		# pick the most recently modified YAML file;
		elglob c "${CONFIG_DIR}/*.y*ml"
		pipeline { stat -c "%Y %n" ${c} }
		pipeline { s6-sort }
		awk "END {print $NF}"
	}
	tee env/CONFIG_FILE
}
if {
	# if TUN is required, ensure that it exists;
	if -nt {
		backtick -D "false" -E disabled { yq eval ".tun.disabled" ${CONFIG_FILE} }
		# "/usr/bin/s6-true" or "/usr/bin/s6-false";
		"s6-${disabled}"
	}
	define TUN "/dev/net/tun"
	if -nt { s6-test -e ${TUN} }
	foreground {
		fdmove -c 1 2
		s6-echo "[WARN] \"${TUN}\" not found. Attempting to \"mknod\"..."
	}
	if {
		backtick -E dirname { s6-dirname ${TUN} }
		s6-mkdir -p ${dirname}
	}
	mknod -m a+w ${TUN} c 10 200
}
# test the CONFIG_FILE and redirect the output to "OUTPUT_DUMP";
foreground {
	redirfd -w 1 env/OUTPUT_DUMP
	fdmove -c 2 1
	execline-cd ${CONFIG_DIR}
	exec -c
	nebula -test -config ${CONFIG_FILE}
}
importas -iu ? ?
# if the test fails, display the errors and exit;
ifelse { s6-test ${?} -ne 0 } {
	fdmove -c 1 2
	foreground { s6-echo "[ERROR] Invalid config file. Aborting." }
	foreground { grep -io "error=.*" env/OUTPUT_DUMP }
	exit ${?}
}
backtick -E S6_CMD_WAIT_FOR_SERVICES_MAXTIME {
	printcontenv S6_CMD_WAIT_FOR_SERVICES_MAXTIME
}
s6-notifyoncheck -t ${S6_CMD_WAIT_FOR_SERVICES_MAXTIME}
execline-cd ${CONFIG_DIR}
exec -c
nebula -config ${CONFIG_FILE}
